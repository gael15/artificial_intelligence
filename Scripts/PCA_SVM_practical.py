#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 13:18:04 2021

@author: guetin gael
"""
# =============================================================================
# Librairies
# =============================================================================
import numpy as np

from numpy.linalg import *

import matplotlib.pyplot as plt

from matplotlib import cm

import pandas as pd

from sklearn.svm import SVC

from sklearn.model_selection import train_test_split

import seaborn as sb

# =============================================================================
# Fonction PCA
# =============================================================================

def PCA(X ,reducedDim=2):

   #Computing of the mean

   X_moy = np.mean(X,axis=0)
   
   #X = X.T
   
   #Shift of matrix

   X_res = X-X_moy

   #Computing of covariance matrix

   #C = (1/3) * np.dot(X_res,X_res.T)

   C = np.cov(X_res,rowvar=False)

   #Decomposition in singular values to extract eigvalues and normalized eigvectors

   #U,S,V = np.linalg.svd(C)
   
   #Extracting of eigvalues and eigvectors:U=evecs and S = evals

   evals, evecs = np.linalg.eigh(C)
   
   #Sort eigvals in decreasing order

   idx = np.argsort(evals)[::-1]

   evals = evals[idx]

   evecs = evecs[:,idx]
 
   #Dimension_rescaled
   evecs = evecs[:,:reducedDim]
   
   #U = U[:,:reducedDim]

   s = np.dot(evecs.T, X_res.T).T
   
   return s#,evals,evecs

    
if __name__ == '__main__' :
    
    #X = np.array([[1, 5, 3, 3],[4, 4, 3, 5]])
    
    #X = np.array([[1.268, 4.732, 3.5, 2.5],[3, 5, 3.134, 4.866]])
    
    data = pd.read_csv("flowerTrain.data" , names =['sepal length','sepal width','petal length','petal width','species'])

    X = pd.DataFrame(data=data)

    X = data.iloc[:,0:4]
    
    # s, evals,evecs = PCA(X ,reducedDim=1)
    
    s = PCA(X ,reducedDim=2)
    y = data.loc[:, "species"]
    columns1 = ['PC1','PC2']
    principal_df = pd.DataFrame(s , columns = columns1[:2])
    principal_df = pd.concat([principal_df , pd.DataFrame(y)] , axis = 1)

    
    plt.figure(1)
    # plt.scatter(X[0,0],X[1,0])
    # plt.scatter(X[0,1],X[1,1])
    # plt.scatter(X[0,2],X[1,2])
    # plt.scatter(X[0,3],X[1,3])
    # plt.xlim(0, 7)
    # plt.ylim(0, 7)
    
    sb.scatterplot(s[:,0],s[:,1],hue="species",data=data)
    # plt.arrow(evecs[0,0],evecs[1,0],evals[0]*evecs[2,0],evecs[3,0],color='red')
    # plt.arrow(evals[1]*evecs[1,0],0,0,evals[1],color='red')
    
    plt.xlabel("PC{}".format(1))
    plt.ylabel("PC{}".format(2))
    plt.show()
    
# =============================================================================
# Support vector Machine :SVM
# =============================================================================
   
    princip = principal_df[['PC1','PC2']].to_numpy()
    type_label = np.where(principal_df["species"]=='interior',0,1)
    model = SVC(kernel='linear',C = 1E10)
    model.fit(princip, type_label)
    
    # w.x + b = 0
    w = model.coef_[0]
    a = -w[0] / w[1]
    xx = np.linspace(0.4, 3)
    yy = a * xx - (model.intercept_[0]) / w[1]
    
    
    b =model.support_vectors_[0]
    yy_down = a * xx + (b[1] - a*b[0]) 
   
    
    b =model.support_vectors_[-1]
    yy_up = a * xx + (b[1] - a*b[0])
    
    plt.figure(2)
    plt.plot(xx, yy)
    plt.plot(xx, yy_down, 'k--')
    plt.plot(xx, yy_up, 'k--')
    sb.scatterplot(s[:, 0], s[:, 1],hue="species",data=data)
    sb.scatterplot(model.support_vectors_[:,0],model.support_vectors_[:,1],s=10,facecolors='red')
    plt.xlabel("PC{}".format(1))
    plt.ylabel("PC{}".format(2))
    plt.show()
    
# =============================================================================
# Classification des nouvelles espèces
# =============================================================================
data = np.array([[5.1, 3.5, 1.4, 0.2],[7.0, 3.2, 4.7, 1.4],[6.4, 3.2, 4.5, 1.5],[6.3, 3.3, 6.0, 2.5],[5.8, 2.7, 5.1, 1.9],[4.9, 3.0, 1.4, 0.2]])

X = pd.DataFrame(data=data)

s = PCA(X ,reducedDim=2)

prediction = model.predict([s[0], s[1]])

print("prediction",prediction)
