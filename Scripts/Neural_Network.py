#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 13:20:50 2021

@author: guetin gael

"""
# =============================================================================
# Librairies
# =============================================================================
import numpy as np

from numpy.linalg import *

import matplotlib.pyplot as plt

import pandas as pd

# =============================================================================
# Functions
# =============================================================================
def sigmoid(z): 
  return 1./(1+np.exp(-z))

def forwardAndBackward(X,t,w):
    
    #Forwardpropagation
    
    h1in=w[0,0]*X[0,0] + w[2,0]*X[1,0]
    h2in=w[1,0]*X[0,0] + w[3,0]*X[1,0]
    
    h1out=sigmoid(h1in)
    h2out=sigmoid(h2in)
  
    
    y1in =w[4,0]*h1out + w[6,0]*h2out
    y2in =w[5,0]*h1out + w[7,0]*h2out
    
    y1out =sigmoid(y1in)
    y2out=sigmoid(y2in)
 
    L = (y1out - t[0,0])**2 + (y2out - t[1,0])**2
    
    #Backpropagation
    
    f1 = (2*y1out - 2*t[0,0]) 
    
    f2 = (2*y2out - 2*t[1,0])
    
    f3 = (2*y1out-2*t[0,0])*sigmoid(y1in)*(1-sigmoid(y1in)) 
     
    f4 = ((2*y2out-2*t[1,0])*sigmoid(y2in)*(1-sigmoid(y2in)))
    
    f5 = w[4,0]*((2*y1out-2*t[0,0])*sigmoid(y1in)*(1-sigmoid(y1in))) +  (w[5,0]*(2*y2out-2*t[1,0])*sigmoid(y2in)*(1-sigmoid(y2in)))#Deux vals avec en bas
    
    f6= w[6,0]*((2*y1out-2*t[0,0])*sigmoid(y1in)*(1-sigmoid(y1in))) +  (w[7,0]*(2*y2out-2*t[1,0])*sigmoid(y2in)*(1-sigmoid(y2in)))
    
    f7 = (w[4,0]*((2*y1out-2*t[0,0])*sigmoid(y1in)*(1-sigmoid(y1in))) +  (w[5,0]*(2*y2out-2*t[1,0])*sigmoid(y2in)*(1-sigmoid(y2in))))*(h1out*(1-h1out))
    
    f8 = f6*(h2out*(1-h2out))
    
    gradW0 = f7*X[0,0]
    
    gradW1 = f8*X[0,0]
    
    gradW2 = f7*X[1,0] 
    
    gradW3 = f8*X[1,0]
    
    gradW4 = f3*h1out                                                                                                                                     
    
    gradW5 = f4*h1out
    
    gradW6 = f3*h2out
    
    gradW7 = f4*h2out
    
    gradW=np.array([[gradW0],[gradW1],[gradW2],[gradW3],[gradW4],[gradW5],[gradW6],[gradW7]])
    
    return L,gradW
 
def minimize_loss(x,t,w,rho,tol =10**(-6),Niter =100):
    x_=np.zeros((2,1))
    t_=np.zeros((2,1))
    x_[0,0] = x[0,0]
    x_[1,0] = x[1,0]
    t_[0,0] = t[0,0]
    t_[1,0] = t[1,0]
    Loss,gradw = forwardAndBackward(x_,t_,w)
    perte = []
    perte.append(Loss)
    for j in range( Niter):
            if Loss > tol :
                w = w - rho*gradw                           
                Loss,gradw = forwardAndBackward(x_,t_,w)
                perte.append(Loss)
            else :
                print("Convergence : Loss = ",Loss)
                break
    return perte, gradw

# =============================================================================
# Main Function
# =============================================================================
    
if __name__ == '__main__' :
    
    #Datas train
    
    #X = np.array([[2],[1]])
    #x = np.array([[-1],[3]])
    x=np.array([[1],[4]])
    
    #t = np.array([[1],[0]])
    t = np.array([[0],[1]])
     
    w = np.array([[2], [-3], [-3], [4], [1], [-1], [0.25],[2]])
    
    L,gradW=forwardAndBackward(x,t,w)
    rho =0.2
    N = np.shape(x)
    gradw = np.zeros((8,N[1]))
    Loss = np.zeros((1,N[1]))
    perte = []
    gradw_tot = []
    epoch =100
    x_=np.zeros((2,1))
    t_=np.zeros((2,1))
    
    for j in range (epoch):
        for i in range (N[1]):
            x_[0,0] = x[0,i]
            x_[1,0] = x[1,i]
            t_[0,0] = t[0,i]
            t_[1,0] = t[1,i]
            Loss[0,i],gr = forwardAndBackward(x_,t_,w)
            gradw[:,i]= gr[:,0]
        
        perte.append(np.mean(Loss))
        gradw_tot.append(np.mean(gradw,axis = 1))
        w = w[:,0] - rho * np.mean(gradw,axis = 1)
        w = w.reshape((8,1))
                 
    plt.figure(1)
    plt.plot(perte)
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.title("Training and testing on three samples")

# =============================================================================
# Training datas
# =============================================================================
    data =pd.read_csv("NNTraining.data")
    x1 = data.iloc[:,0]
    x2 = data.iloc[:,1]
    t1 = data.iloc[:,2]
    t2 = data.iloc[:,3]
    
    #w = np.array([[2],[-3],[-3],[4],[1],[-1],[0.25],[2]])
    rho =1.8
    x1 = x1.to_numpy()
    x2 = x2.to_numpy()
    t1 = t1.to_numpy()
    t2 = t2.to_numpy()
    
    x1 = x1[:].reshape((200,1))
    x2 = x2[:].reshape((200,1))
    t1 = t1[:].reshape((200,1))
    t2 = t2[:].reshape((200,1))
    
    X_train = np.concatenate((x1,x2),axis = 1)
    T_train = np.concatenate((t1,t2),axis = 1)
    X_train = X_train.T
    T_train= T_train.T
    
    Element_shape = np.shape(X_train)
    perte = []
    perte_bach = []
    gradw_tot = []
    x_=np.zeros((2,1))
    t_=np.zeros((2,1))
    
    bach = 50
    epoch =10
    N =epoch
    bach_size = int(Element_shape[1]/bach)
    m = bach_size
        
    gradw = np.zeros((8,m))
    Loss = np.zeros((1,m))
      
    gradw_bach = np.zeros((8,bach))
    for j in range (N):
        for i in range (bach):
            for a in range (m):
                x_[0,0] = X_train[0,i*m+a]
                x_[1,0] = X_train[1,i*m+a]
                t_[0,0] = T_train[0,i*m+a]
                t_[1,0] = T_train[1,i*m+a]
                Loss[0,a],gr = forwardAndBackward(x_,t_,w)
                gradw[:,a]= gr[:,0]
            gradw_bach = np.mean(gradw,axis = 1)
            perte_bach.append(np.mean(Loss))
            w = w[:,0] - rho * gradw_bach 
            w = w.reshape((8,1))
        perte.append(np.mean(perte_bach))
      
    plt.figure(2)
    plt.plot(perte)
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.title("Training")
# =============================================================================
# Test datas : Before and after training
# =============================================================================

    data_test =pd.read_csv("NNTest.data")
    x1_test = ((data_test.iloc[:,0]).to_numpy()).reshape((100,1))
    x2_test = ((data_test.iloc[:,1]).to_numpy()).reshape((100,1))
    t1_test = ((data_test.iloc[:,2]).to_numpy()).reshape((100,1))
    t2_test = ((data_test.iloc[:,3]).to_numpy()).reshape((100,1))
    
    x_Test = np.concatenate((x1_test,x2_test),axis = 1)
    t_Test = np.concatenate((t1_test,t2_test),axis = 1)
    x_Test = x_Test.T
    t_Test = t_Test.T
    w_train = w
    w_before = np.array([[2],[-3],[-3],[4],[1],[-1],[0.25],[2]])
    Element_shape = np.shape(x_Test)
    Perte_test = []
    Perte_before = []
    for i in range (Element_shape[1]):
        x_[0,0] = x_Test[0,i]
        x_[1,0] = x_Test[1,i]
        t_[0,0] = t_Test[0,i]
        t_[1,0] = t_Test[1,i]
        Loss, p = forwardAndBackward(x_,t_,w_train)
        Loss_before, p_befor = forwardAndBackward(x_,t_,w_before)
        Perte_test.append(Loss)
        Perte_before.append(Loss_before)
   
    plt.figure(3)
    plt.plot(Perte_before)
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.title("Before training")
    
    plt.figure(4)
    plt.plot(Perte_test)
    plt.xlabel("Epochs")
    plt.ylabel("Loss")
    plt.title("After training")
    
# =============================================================================
# End
# =============================================================================
